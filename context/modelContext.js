import React from 'react';

const modelContext = React.createContext({
    dataModel: {
        modelo: '123456789'
    },
    login: () => {},
    logout: () => {},
});
export default modelContext;
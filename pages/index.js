import React, { useState, useEffect } from "react";
import modelContext from "../context/modelContext";
import * as tf from "@tensorflow/tfjs";
import Display from "./display";
import WebcamFace from "./WebcanFace";
import WebcamDNI from "./WebcamDNI";
import Inicio from "./Inicio";
import { BarsSpinner } from "react-spinners-kit";
import * as facemesh from "@tensorflow-models/face-landmarks-detection";
const MODEL_URL = "model/model.json";

function Index() {
  const [bolCargandoModel, setbolCargandoModel] = useState(false);
  const [netModel, setNetmodel] = useState(null);
  const [netModelDni, setNetmodelDni] = useState(null);
  const [showInicio, setShowInicio] = useState(true);
  const [showWebCamFace, setShowWebCamFace] = useState(false);
  const [showWebCamDni, setShowWebCamDni] = useState(false);
  const [dataCode, setdataCode] = useState({ name: "", code: "" });

  const runCoco = async () => {
    setbolCargandoModel(true);
    const net = await facemesh.load(
      facemesh.SupportedPackages.mediapipeFacemesh
    );
    const netDni = await tf.loadGraphModel(MODEL_URL);
    setNetmodel(net);
    setNetmodelDni(netDni);
    setbolCargandoModel(false);
  };

  useEffect(() => {
    runCoco();
  }, []);

  const fnShowWebCamFace = () => {
    setShowInicio(false);
    setShowWebCamFace(true);
  };

  const fnShowWebCamDni = (nameImgFace, codeImg) => {
    setdataCode({ name: nameImgFace, code: codeImg });
    setShowWebCamFace(false);
    setShowWebCamDni(true);
  };

  const fnHideWebCamDni = () => {
    setShowWebCamDni(false);
  };

  return (
    <div
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      {/* <Display flag={siguiente} fnDemo={mostrarAlert} /> */}

      {showInicio ? <Inicio fnShowface={fnShowWebCamFace} /> : ""}

      {showWebCamFace
        ? [
            netModel ? (
              <WebcamFace
                xModel={netModel}
                key="faceid"
                fnShowDni={fnShowWebCamDni}
              />
            ) : (
              <div>Cargando modelo face....</div>
            ),
          ]
        : ""}
      {showWebCamDni
        ? [
            netModelDni ? (
              <WebcamDNI
                xModel={netModelDni}
                key="scanDni"
                xData={dataCode}
                fnHideDni={fnHideWebCamDni}
              />
            ) : (
              ""
            ),
          ]
        : ""}

      {bolCargandoModel ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            marginTop: "100px",
          }}
        >
          <BarsSpinner
            size={30}
            // backColor="#f8d614"
            // frontColor="#323192"
            color="#323192"
            loading={true}
          />
          <br />
          cargando modelos...
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default Index;

import React, { useContext, useState } from "react";
import modelContext from "../context/modelContext";

function Display(props) {
  const miModel = useContext(modelContext);
  const [flag, setFlag] = useState(props.flag);

  console.log("props::::", props);

  console.log("flag hijo::", flag);
  return (
    <div>
      <h1>Resultados finales</h1>
      <div>flag padre {props.flag ? "true" : "false"}</div>

      <div>flag hijo {flag ? "true" : "false"}</div>

      <button onClick={() => props.fnDemo()}>ver DNI</button>
    </div>
  );
}

export default Display;

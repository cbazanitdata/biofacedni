import React, { useRef, useEffect, useState } from "react";
import * as tf from "@tensorflow/tfjs";
import Webcam from "react-webcam";
import { drawRect } from "../utilities/utilities";
import moment from "moment";
import Swal from "sweetalert2";
import { GuardSpinner } from "react-spinners-kit";

import styles from "../styles/WebcamFace.module.css";
import { useRouter } from "next/router";

function WebcamDNI(props) {
  const router = useRouter();
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);
  const canvasTextRef = useRef(null);
  const [bolCargando, setbolCargando] = useState(false);
  const [bolCargandoModel, setbolCargandoModel] = useState(false);
  const [bolProcesando, setbolProcesando] = useState(true);

  const queryParams = router.query;
  const q = props?.xData?.name;
  const code = props?.xData?.code;

  const videoConstraints = {
    facingMode: "environment",
  };

  let contador = 0;
  var refreshIntervalId;
  alert(tf.getbackend());

  const runCoco = async () => {
    setbolCargandoModel(true);
    // const net = await tf.loadGraphModel(MODEL_URL);
    const net = props.xModel;
    setbolCargandoModel(false);

    refreshIntervalId = setInterval(() => {
      dibujarTexto();
      console.log("bolProcesando>>>", bolProcesando);
      if (bolProcesando) {
        detect(net);
      }
    }, 1000);
  };

  const dibujarTexto = () => {
    const videoWidth = webcamRef.current.video.videoWidth;
    const videoHeight = webcamRef.current.video.videoHeight;
    canvasTextRef.current.width = videoWidth;
    canvasTextRef.current.height = videoHeight;
    const cxt1 = canvasTextRef.current.getContext("2d");

    const texto = "Captura frontal del documento";
    cxt1.beginPath();

    cxt1.fillStyle = "#00FFFF";
    cxt1.fillRect(160, 20, 310, 50);

    cxt1.fillStyle = "white";
    cxt1.font = "bold 20px arial";
    cxt1.fillText(texto, 170, 50);

    // texto de abajo
    const texto1 = "Coloca el documento frente a la cámara";
    cxt1.fillStyle = "#778899";
    cxt1.fillRect(0, 420, videoWidth, 60);

    cxt1.fillStyle = "white";
    cxt1.font = "bold 18px arial";
    cxt1.fillText(texto1, 160, 450);

    // var grd = cxt1.createLinearGradient(0, 20, 600, 50);
    // grd.addColorStop(0, "black");
    // grd.addColorStop(1, "white");
    // cxt1.fillStyle = grd;

    // para hacerlo horizontal abajo ============
    // cxt1.rotate((90 * Math.PI) / 180);
    // cxt1.fillText(texto, 100, -100);
    //  =========================================
  };

  const detect = async (net) => {
    // Check data is available
    console.log("entro al detect");
    if (
      typeof webcamRef.current !== "undefined" &&
      webcamRef.current !== null &&
      webcamRef.current.video.readyState === 4
    ) {
      console.log("entro al get video");
      // Get Video Properties
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current.video.videoWidth;
      const videoHeight = webcamRef.current.video.videoHeight;

      // Set video width
      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      // Set canvas height and width
      canvasRef.current.width = videoWidth;
      canvasRef.current.height = videoHeight;
      const img = tf.browser.fromPixels(video);
      const resized = tf.image.resizeBilinear(img, [640, 480]);
      const casted = resized.cast("int32");
      const expanded = casted.expandDims(0);
      const obj = await net.executeAsync(expanded);

      // const boxes = await obj[4].array();
      // const classes = await obj[6].array();
      // const scores = await obj[7].array();

      const boxes = await obj[6].array();
      const classes = await obj[4].array();
      const scores = await obj[5].array();

      const ctx = canvasRef.current.getContext("2d");

      requestAnimationFrame(() => {
        drawRect(
          boxes[0],
          classes[0],
          scores[0],
          0.8,
          videoWidth,
          videoHeight,
          ctx
        );
      });

      // codigo para capturar imagen y enviarlo al S3 por backend
      let boxes2 = boxes[0];
      let classes2 = classes[0];
      let scores2 = scores[0];
      let threshold = 0.8;
      console.log("antes del if de tomar foto");
      console.log("contador::", contador);
      console.log("scores2>>", scores2);

      for (let i = 0; i <= boxes2.length; i++) {
        if (boxes2[i] && classes2[i] && scores2[i] > threshold)
          contador = contador + 1;

        if (
          boxes2[i] &&
          classes2[i] &&
          scores2[i] > threshold &&
          contador === 3
        ) {
          try {
            setbolProcesando(false);
            console.log("tomar foto dni");
            clearInterval(refreshIntervalId);
            const canvas = document.createElement("canvas");
            canvas.width = videoWidth;
            canvas.height = videoHeight;
            canvas.getContext("2d").drawImage(webcamRef.current.video, 0, 0);
            const img = document.createElement("img");
            img.src = canvas.toDataURL("image/webp");
            // document.getElementById("screenshot").appendChild(img);
            setbolCargando(true);
            let imageBlob = await new Promise((resolve) =>
              canvas.toBlob(resolve, "image/png")
            );

            const nomImg =
              "dni" + moment().format("YYYYMMDD") + "-" + code + ".png";
            let formData = new FormData();
            formData.append("firstName", "John");
            formData.append("imgFace", imageBlob, nomImg);
            let urlBackend = "https://val-biomet.qa.vmas.com.pe/uploadFile";
            // let urlBackend = "https://a0c8-181-65-1-194.ngrok.io/uploadFile";
            let response = await fetch(urlBackend, {
              method: "POST",
              body: formData,
            });
            let result = await response.json();
            console.log("result::", result);
            Swal.fire({
              title: "Validación!",
              text: "el reconocimiento de DNI fue exitoso.",
              icon: "success",
              confirmButtonText: "Cool",
            }).then((res) => {
              setbolCargando(false);
              //   navigate("/Resultado?img1=" + q + "&img2=" + nomImg, {
              //     replace: true,
              //   });
              // setbolProcesando(true);
              props.fnHideDni();
              router.push("/Resultado?img1=" + q + "&img2=" + nomImg);
            });
          } catch (error) {
            console.log("error:", error);
            setbolCargando(false);
            // setbolProcesando(true);
          }
        }
      }

      tf.dispose(img);
      tf.dispose(resized);
      tf.dispose(casted);
      tf.dispose(expanded);
      tf.dispose(obj);
    }
  };

  useEffect(() => {
    runCoco();
  }, []);

  return (
    <div className={styles.App}>
      {/* <div>
        {q ? <span>el codigo es {q}</span> : <span>No hay codigo</span>}
      </div> */}
      {/* <div className="">Enfoca tu DNI y espera un momento.</div> */}
      <header>
        {bolCargandoModel ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <GuardSpinner size={40} color="#686769" loading={true} />
            <span style={{ marginTop: "10px" }}>Cargando Modelos...</span>
          </div>
        ) : (
          <Webcam
            ref={webcamRef}
            muted={true}
            className="videoDNICSS"
            videoConstraints={videoConstraints}
          />
        )}
        <canvas
          ref={canvasRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 8,
            width: "100%",
            height: "100%",
          }}
        />{" "}
        <canvas
          ref={canvasTextRef}
          style={{
            position: "absolute",
            zindex: 9,
            left: 0,
            right: 0,
            width: "100%",
            height: "100%",
          }}
        />
      </header>
      {bolCargando ? (
        <GuardSpinner size={40} color="#686769" loading={true} />
      ) : null}{" "}
    </div>
  );
}

export default WebcamDNI;

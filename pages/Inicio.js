import React from "react";
import styles from "../styles/Inicio.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";

function Inicio(props) {
  console.log("ver props inicial:::", props);
  const router = useRouter();

  return (
    <div className={styles.contenedor_inicio}>
      <div className={styles.ini_logo}>
        <Image
          src="/logoVendemas.jpg"
          alt="logo vendemas niubiz"
          width="180px"
          height="120px"
        />
      </div>
      <div className={styles.ini_titulo}>
        Te damos la bienvenida a la demo de Biometria Vendemas
      </div>
      <div className={styles.ini_subtitulo}>
        Realiza un enrolamiento para acceder a todas las opciones que Biometria
        puede ofrecer
      </div>

      <div
        className={styles.ini_tarjeta}
        style={{
          display: "flex",
          flexDirection: "row",
          //   alignItems: "center",
        }}
        onClick={() => {
          props.fnShowface();
        }}
      >
        <div className={styles.tarjeta_logo}>
          <Image src="/faceid.jpg" width="100px" height="100px" alt="faceid" />
          {/* <img src="./faceid.jpg" alt="face" style={{ width: "100px" }} /> */}
        </div>
        <div>
          <div className={styles.tarjeta_titulo}>Onboarding</div>
          <div className={styles.tarjeta_contenido}>
            Enrolate a travez del proceso de verificaicion de indentidad
          </div>
        </div>
      </div>
      <div style={{ marginTop: "25px" }}>
        <button
          className={styles.button43}
          onClick={() => {
            router.push("/");
          }}
        >
          CONTÁCTANOS
        </button>
      </div>
    </div>
  );
}

export default Inicio;

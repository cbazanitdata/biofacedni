import moment from "moment";
import Swal from "sweetalert2";
import React, { useRef, useEffect, useState } from "react";
import { GuardSpinner } from "react-spinners-kit";
import * as facemesh from "@tensorflow-models/face-landmarks-detection";
import "@tensorflow/tfjs-core";
import "@tensorflow/tfjs-converter";
import "@tensorflow/tfjs-backend-webgl";
import "@tensorflow/tfjs-backend-cpu";

import { drawMesh } from "../utilities/utilities";
import styles from "../styles/WebcamFace.module.css";
import Webcam from "react-webcam";
import { useRouter } from "next/router";

function WebcamFace(props) {
  const router = useRouter();
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);
  const [bolCargando, setbolCargando] = useState(false);
  const [bolCargandoModel, setbolCargandoModel] = useState(false);

  let bolControl = true;
  let contador = 0;

  var refreshIntervalId;
  const runFacemesh = async () => {
    setbolCargandoModel(true);
    // const net = await facemesh.load(
    //   facemesh.SupportedPackages.mediapipeFacemesh
    // );
    const net = props.xModel;
    setbolCargandoModel(false);

    refreshIntervalId = setInterval(() => {
      console.log(new Date(), "****", bolControl);
      if (bolControl) {
        detect(net);
      }
    }, 1000);
  };

  const detect = async (net) => {
    if (
      typeof webcamRef.current !== "undefined" &&
      webcamRef.current !== null &&
      webcamRef.current.video.readyState === 4
    ) {
      // Get Video Properties
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current.video.videoWidth;
      const videoHeight = webcamRef.current.video.videoHeight;

      // Set video width
      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      // Set canvas width
      canvasRef.current.width = videoWidth;
      canvasRef.current.height = videoHeight;
      // contador = contador + 1;
      const face = await net.estimateFaces({ input: video });

      // Get canvas context
      const ctx = canvasRef.current.getContext("2d");
      requestAnimationFrame(() => {
        drawMesh(face, ctx);
      });

      if (face[0].faceInViewConfidence) {
        if (face[0].faceInViewConfidence > 0.9) contador = contador + 1;

        console.log("contador", contador);
        if (face[0].faceInViewConfidence > 0.9 && contador === 5) {
          bolControl = false;
          clearInterval(refreshIntervalId);
          console.log(new Date(), "===", bolControl);
          console.log("tomar foto");

          // ==============================este codigo toma foto con video ============================================================

          setbolCargando(true);
          const canvas = document.createElement("canvas");
          canvas.width = videoWidth;
          canvas.height = videoHeight;
          canvas.getContext("2d").drawImage(webcamRef.current.video, 0, 0);
          const img = document.createElement("img");
          img.src = canvas.toDataURL("image/webp");
          // document.getElementById("screenshot").appendChild(img);
          setbolCargando(true);
          let imageBlob = await new Promise((resolve) =>
            canvas.toBlob(resolve, "image/png")
          );

          const codeImg = makeid(10);
          const nomImg =
            "face" + moment().format("YYYYMMDD") + "-" + codeImg + ".png";
          let formData = new FormData();
          formData.append("firstName", "John");
          formData.append("imgFace", imageBlob, nomImg);
          let urlBackend = "https://val-biomet.qa.vmas.com.pe/uploadFile";

          try {
            let response = await fetch(urlBackend, {
              method: "POST",
              body: formData,
            });
            let result = await response.json();
            console.log("result::", result);
            Swal.fire({
              title: "Validación!",
              text: "el reconocimiento fue exitoso.",
              icon: "success",
              confirmButtonText: "Cool",
            }).then((res) => {
              setbolCargando(false);
              // router.push("/WebcamDNI?q=" + nomImg + "&code=" + codeImg);
              props.fnShowDni(nomImg, codeImg);
              bolControl = true;
            });
          } catch (error) {
            console.log("error:", error);
            setbolCargando(false);
            bolControl = true;
          }
        }
      }
    }
  };

  const makeid = (length) => {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  useEffect(() => {
    runFacemesh();
  }, []);

  return (
    <div
      className={styles.App}
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <div className="cameraa" style={{ display: "flex", margin: "10px" }}>
        <Webcam
          ref={webcamRef}
          screenshotFormat="image/png"
          className={styles.videoCircle}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 300,
            height: 300,
          }}
        />
        {bolCargando ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <GuardSpinner size={40} color="#686769" loading={true} />
          </div>
        ) : null}

        <canvas
          ref={canvasRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 300,
            height: 300,
          }}
        />
      </div>

      <div style={{ width: "400px" }}>
        {bolCargandoModel ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <GuardSpinner size={40} color="#686769" loading={true} />
            <span style={{ marginTop: "10px" }}>Cargando Modelos...</span>
          </div>
        ) : (
          <div className={styles.textNotificationCamera}>
            Mantenga fija la mirada hacia cámara para veritificar tu identidad
          </div>
        )}
      </div>

      {/* <div id="screenshot" style={{ display: "none" }}></div> */}
      <div id="screenshot"></div>
    </div>
  );
}

export default WebcamFace;

import React, { useEffect, useState } from "react";
import { RotateSpinner } from "react-spinners-kit";
import { useRouter } from "next/router";
import Image from "next/image";
import styles from "../styles/Inicio.module.css";

function Resultado() {
  const router = useRouter();

  const queryParams = router.query;
  const img1 = queryParams.img1;
  const img2 = queryParams.img2;

  const [bolEstado, setBolEstado] = useState(false);
  const [bolProcesando, setbolProcesando] = useState(true);
  const [respuesta, setRespuesta] = useState("");
  const [porcRes, setPorcRes] = useState(0);

  let msj = "xxx";

  const runCoco = async () => {
    let elBody = {
      img1,
      img2,
    };
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(elBody),
    };
    let urlBackend = "https://val-biomet.qa.vmas.com.pe/checkFaces";
    let response = await fetch(urlBackend, requestOptions);
    let result = await response.json();
    console.log("result::", result);
    console.log("result.success::", result.success);
    setbolProcesando(false);
    if (result.success) {
      if (result.data) {
        setRespuesta(
          "Resultado del reconocimiento : " + Math.round(result.data)
        );
        setBolEstado(true);
      } else {
        setRespuesta("No se reconoce : " + Math.round(result.data));
        setBolEstado(false);
      }
    } else {
      setRespuesta("Ocurrio un error");
      setBolEstado(false);
    }
  };

  useEffect(() => {
    runCoco();
  }, []);

  return (
    <div>
      <span key="s1">
        <h1>Status Reconocimiento </h1>
      </span>
      <span key="s2">{img1}</span>
      <br />
      <span key="s3">{img2}</span>
      <br />
      <h3>{respuesta} %</h3>

      <br />

      {bolProcesando ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
          key="11"
        >
          <RotateSpinner size={150} color="#BCA9F5" loading={true} />
        </div>
      ) : (
        [
          bolEstado ? (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
              key="22"
            >
              <Image
                src="/check.png"
                alt="ok"
                width="200px"
                height="200px"
                key="1"
              />
              {/* <img src={check} alt="ok" width="200px" key={"1"} /> */}
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
              key="33"
            >
              <Image
                src="/error.png"
                alt="error"
                width="200px"
                height="200px"
                key="2"
              />
              {/* <img src={errorPng} alt="error" width="200px" key={"2"} /> */}
              <br />
              <button
                className={styles.button43}
                onClick={() => {
                  router.push("/");
                }}
              >
                Volver
              </button>
            </div>
          ),
        ]
      )}
    </div>
  );
}

export default Resultado;
